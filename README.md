# Savvy.io Frontend coding challenge

## Objective
A React wallet that can handle different currencies and exchange them between each other.
It also includes other features: making deposits, select default currency, display total value of the accounts in the selected currency.

## Features summary
- Exchange between currencies
    - possibility to calculate any of the exchanging / receiving amounts
    - automatically create an account with the receiving currency if needed
    - error message if the user doesn't have an account with exchanging currency
    - error message if the corresponding account doesn't have enough funds
- Deposit a currency
    - automatically create an account with the deposit if needed
- Show balance of each currency
- Show totalised value (across all your currencies) in the user's default currency
- Set user's default currency
    - exchange and deposit features are updated automatically when the default currency is changed

## Technologies summary

React.js, Redux, ES6, Jest, Enzyme, Bootstrap

## Installation and usage

1. Install required npm packages: 
```sh
$ npm install
```

2. Start the application in development mode: 
```sh
$ npm start
```

3. (Optional) Run application tests with or without code coverage:
```sh
$ npm test
```
```
$ npm run test-coverage
```

4. (Optional) Build the static assets optimized for deployment: 
```sh
$ npm run build
```


## What's inside?

I've started the project using Facebook's official package for building React apps: 
https://github.com/facebookincubator/create-react-app

React version used is 16.8 and you’ll need to have Node 8.10.0 or later on development machine.

Other packages installed:
- [React Router](https://reacttraining.com/react-router/) - provide multi-page functionality for SPA built with React
- [react-redux](https://github.com/reduxjs/react-redux) - bindings for React to use Redux model as a state container
- [redux-thunk](https://github.com/reduxjs/redux-thunk) - to allow action creators to perform asynchronous dispatch 
- [Jest](http://jestjs.io/) - library for javascript testing
- [jest-fetch-mock](https://github.com/jefflau/jest-fetch-mock) - utility used when testing Async Action Creators to mock the browser fetch
- [redux-mock-store](https://github.com/dmitry-zaets/redux-mock-store) - utility used to mock Redux store useful when testing Action Creators that use getState to retrieve data from store
- [react-bootstrap](https://react-bootstrap.github.io/) - UI components implemented using Bootstrap library
- [react-select](https://react-select.com) - Select control with various features like searchable input, autocomplete, etc

## Project Structure

    |-- build    # Generated static files for deployment
    |-- public   # Deployment files are copied and then served from here
    |-- src
        |-- components         # Generic components to be reused within the application
        |-- features           # The app features are groupped in the corresponding folders
            |-- accounts            # module handling display balance of each account, display totalised value and deposit 
                |-- components          # folder containing feature related components and sub-components
                |-- accountsActions.js  # file dedicated to action creators
                |-- accountsReducer.js  # file dedicated to action types, reducer and selectors
            |-- exchange            # module handling currency converter and the exchange rates (needed internally)
            |-- user                # module handling user profile display and set user default currency
        |-- redux
            |-- rootReducer.js          # rootReducer used to combine existing reducers (sub-stores)
            |-- store.js                # here the single store is created and middleware is applied (edux-thunk)
        |-- services
            |-- fetchManager.js         # wrapper for fetch that handles parsing JSON and resolves or rejects a promise based on response status
        |-- App.jsx                 # here is done wrapping of the app with Redux Provider and Bootstrap Container
                                    # this is usually expanded with Intl feature, authentication, etc
        |-- DashboardScreen.jsx     # here main feature components are being rendered after initial data fetching is done
                                    # when the app grows to multiple screens, a folder /screens should be added
        |-- setupTests.js           # configure and set up required test utilities

## Architecture decisions

- All components are functional stateless components.
    - 'useEffect' hook: instead of lifecycle methods (eg: componentDidMount for initial service call or listening for changes to defaultCurrency)
    - 'useState' and 'useReducer' hooks: instead of local state (eg: loading, errors, calculate exchange, form data, etc)

- Data from the Redux store is accessed using reducers that are located together with the corresponding data. If a selector needs data from another sub-store then it's also accessed through the other's store selector.

- Project is structured by features (having "smart" Components, Actions, Reducers in the same folder) for scalability and ease of development.
Other new modules could be added directly within 'feature' folder and they can render or be rendered in the existing ones or groupped into different screens.
