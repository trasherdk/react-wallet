import React from 'react';
import { Provider } from 'react-redux';
import store from './redux/store';
import { Container } from 'react-bootstrap';
import DashboardScreen from './DashboardScreen';

const App = () => (
    <Provider store={store}>
        <Container className="mt-3 mb-3">
            <h1>React Wallet</h1>
            <DashboardScreen />
        </Container>
    </Provider>
);

export default App;