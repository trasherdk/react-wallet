import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import CurrencyDropdown from './CurrencyDropdown';

const MoneyInput = ({ currencies, selectedCurrency, amount, onChange, clearOnFocus}) => {
    const inputRef = useRef();

    const getInputValue = amount => amount > 0 ? Math.round(amount * 100) / 100 : '';

    const handleChange = currency => {
        onChange({ currency })
    }

    const handleAmountChange = event => {
        onChange({ amount: Number(event.target.value) })
    }

    return (
        <div className="d-flex">
            <input
                className="card mr-1"
                style={{ flex: 1 }}
                type="number"
                step=".01"
                ref={inputRef}
                value={getInputValue(amount)} 
                onFocus={clearOnFocus ? () => inputRef.current.value = '' : null}
                onBlur={clearOnFocus ? () => inputRef.current.value = getInputValue(amount) : null}
                onChange={handleAmountChange}
            />
            <CurrencyDropdown
                value={selectedCurrency}
                options={currencies}
                onChange={handleChange}
            />
        </div>
    )
};

MoneyInput.propTypes = {
    currencies: PropTypes.array.isRequired,
    selectedCurrency: PropTypes.string,
    amount: PropTypes.number,
    onChange: PropTypes.func.isRequired,
    clearOnFocus: PropTypes.bool,
};

export default MoneyInput;