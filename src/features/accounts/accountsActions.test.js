import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import {
    GET_ACCOUNTS_PENDING,
    GET_ACCOUNTS_SUCCESS,
    GET_ACCOUNTS_FAILED,
    SET_DEPOSIT_SUCCESS,
    SET_WITHDRAWAL_SUCCESS,
    initialState,
} from './accountsReducer';
import {
    getAccounts,
    deposit,
    withdraw,
    GET_ACCOUNTS_ERROR_MESSAGE,
} from './accountsActions';

const mockStore = configureStore([thunk]);

describe('getAccounts', () => {
    const mockAccounts = [{ currency: 'USD', amount: 100 }];

    beforeEach(() => {
        fetch.resetMocks()
    });

    it('should dispatch GET_ACCOUNTS_PENDING and GET_ACCOUNTS_SUCCESS', () => {
        const store = mockStore({
            accountList: initialState,
        });
        const expectedActions = [
            { type: GET_ACCOUNTS_PENDING },
            { type: GET_ACCOUNTS_SUCCESS, payload: mockAccounts },
        ];
        fetch.mockResponseOnce(JSON.stringify(mockAccounts));

        return store.dispatch(getAccounts())
            .then(() => {
                const actions = store.getActions();
                expect(actions).toEqual(expectedActions);
            })
    });

    it('should dispatch GET_ACCOUNTS_FAILED when there is a server error', () => {
        const store = mockStore({
            users: initialState,
        });
        const expectedActions = [
            { type: GET_ACCOUNTS_FAILED, payload: GET_ACCOUNTS_ERROR_MESSAGE },
        ];
        fetch.mockResponseOnce(null, { status: 500 });

        return store.dispatch(getAccounts())
            .then(() => {
                const actions = store.getActions();
                expect(actions).toEqual(expect.arrayContaining(expectedActions));
            })
    });

    it('should dispatch GET_ACCOUNTS_FAILED when fetch is rejected', () => {
        const store = mockStore({
            users: initialState,
        });
        const expectedActions = [
            { type: GET_ACCOUNTS_FAILED, payload: GET_ACCOUNTS_ERROR_MESSAGE },
        ];
        fetch.mockRejectOnce();

        return store.dispatch(getAccounts())
            .then(() => {
                const actions = store.getActions();
                expect(actions).toEqual(expect.arrayContaining(expectedActions));
            })
    });
});

// last two test suites should be updated to use fetch mock when the 
// action creators would call real API instead of a mock function

describe('deposit', () => {
    const mockData = { currency: 'USD', amount: 100 };

    it('should dispatch SET_DEPOSIT_SUCCESS', () => {
        const store = mockStore({
            accountList: initialState,
        });
        const expectedActions = [
            { type: SET_DEPOSIT_SUCCESS, payload: mockData },
        ];

        return store.dispatch(deposit(mockData.amount, mockData.currency))
            .then(() => {
                const actions = store.getActions();
                expect(actions).toEqual(expectedActions);
            })
    });
});

describe('withdraw', () => {
    const mockData = { currency: 'USD', amount: 100 };

    it('should dispatch SET_WITHDRAWAL_SUCCESS', () => {
        const store = mockStore({
            accountList: initialState,
        });
        const expectedActions = [
            { type: SET_WITHDRAWAL_SUCCESS, payload: mockData },
        ];

        return store.dispatch(withdraw(mockData.amount, mockData.currency))
            .then(() => {
                const actions = store.getActions();
                expect(actions).toEqual(expectedActions);
            })
    });
});
