import React from 'react';
import { shallow } from 'enzyme';
import { Deposit } from './Deposit';

describe('<Deposit />', () => {
    const defaultProps = {
        currencies: ['USD', 'EUR'],
        defaultCurrency: 'USD',
        deposit: jest.fn(() => Promise.resolve()),
    };

    it('renders <MoneyInput /> and <Button /> components', () => {
        const wrapper = shallow(<Deposit {...defaultProps} />);

        expect(wrapper.find('Button').exists()).toBe(true);
        expect(wrapper.find('MoneyInput').exists()).toBe(true);
    });

    it('calls deposit function with correct arguments when Button is clicked', () => {
        const wrapper = shallow(<Deposit {...defaultProps} />);
        const inputWrapper = wrapper.find('MoneyInput').dive();
        const mockAmount = 100;

        expect(defaultProps.deposit.mock.calls.length).toBe(0);

        inputWrapper.find('input').simulate('change', {target: { value: mockAmount }});
        wrapper.find('Button').simulate('click');

        expect(defaultProps.deposit.mock.calls.length).toBe(1);
        expect(defaultProps.deposit).toBeCalledWith(mockAmount, defaultProps.defaultCurrency);
    });
});
