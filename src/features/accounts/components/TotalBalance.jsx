import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import MoneyDisplay from '../../../components/MoneyDisplay';
import { selectTotalBalance } from '../accountsReducer';

const TotalBalance = ({ data }) => (
    <Row className="mt-5">
        <Col xs={12}>
            <h2>Total balance</h2>
        </Col>
        <MoneyDisplay amount={data.amount} currency={data.currency} />
    </Row>
);

TotalBalance.propTypes = {
    data: PropTypes.shape({
        amount: PropTypes.number.isRequired,
        currency: PropTypes.string.isRequired,
    }),
};

const mapStateToProps = state => ({
    data: selectTotalBalance(state),
});

export default connect(
    mapStateToProps,
    {}
)(TotalBalance);
