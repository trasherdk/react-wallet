import React, { useState, useReducer, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col, Button, Spinner } from 'react-bootstrap';
import MoneyInput from '../../../components/MoneyInput';
import ErrorMessage from '../../../components/ErrorMessage';
import { getReceivingInitialCurrency } from '../utils';
import { selectExchangeRates, selectCurrencies } from '../exchangeReducer';
import { validateExchange } from '../exchangeActions';
import { selectDefaultCurrency } from '../../user/userReducer';

const EXCHANGING = 'exchanging';
const RECEIVING = 'receiving';

const CurrencyExchange = ({ rates, currencies, defaultCurrency, validateExchange }) => {
    const initialExchangeState = { 
        [EXCHANGING]: {
            amount: 0,
            currency: defaultCurrency,
            id: EXCHANGING,
        },
        [RECEIVING]: {
            amount: 0,
            currency: getReceivingInitialCurrency(defaultCurrency, currencies),
            id: RECEIVING,
        },
        calculated: RECEIVING,
    };

    const calculateExchangeAmount = (amount, currency, exchangeCurrency) => 
        amount * rates[exchangeCurrency] / rates[currency];

    const getNewState = (exchangeStatus, sourceObj, targetObj) => ({
        ...exchangeStatus,
        [sourceObj.id]: sourceObj,
        [targetObj.id]: {
            ...targetObj,
            amount: calculateExchangeAmount(
                sourceObj.amount,
                sourceObj.currency,
                targetObj.currency,
            ),
        },
        calculated: targetObj.id,
    });

    const [exchangeStatus, setExchangeStatus] = useReducer((exchangeStatus, action) => {
        const { type, ...data } = action;
        let target = exchangeStatus.calculated;
        let source = target === EXCHANGING ? RECEIVING : EXCHANGING;

        const getSubState = key => key !== type
            ? exchangeStatus[key]
            : {
                ...exchangeStatus[key],
                ...data
            };

        const sourceObj = getSubState(source);
        const targetObj = getSubState(target);

        if (type === target && data.amount) {
            return getNewState(exchangeStatus, targetObj, sourceObj)
        }
        
        return getNewState(exchangeStatus, sourceObj, targetObj);

    }, initialExchangeState);

    const isExchangeLoading = false;
    const exchangeError = false;

    const [ isLoading, setExchangeLoading ] = useState(isExchangeLoading);
    const [ error, setError ] = useState(exchangeError);

    useEffect(
        () => setExchangeStatus({
            type: EXCHANGING,
            currency: defaultCurrency,
        }),
        [defaultCurrency]
    );

    const handleSubmit = () => {
        setExchangeLoading(true);
        validateExchange(exchangeStatus)
            .then(({ error }) => setError(error))
            .then(() => setExchangeLoading(false));
    };

    return (
        <Row className="mt-5">
            <Col xs={12}>
                <h2>Exchange currency</h2>
            </Col>
            <Col xs={12} sm={5} md={5} lg={3} className="mb-2">
                <MoneyInput 
                    currencies={currencies}
                    selectedCurrency={exchangeStatus.exchanging.currency}
                    amount={exchangeStatus.exchanging.amount}
                    onChange={data => setExchangeStatus({ ...data, type: EXCHANGING })}
                    clearOnFocus={exchangeStatus.calculated === EXCHANGING}
                />
                {error && <ErrorMessage message={error} />}
            </Col>
            <Col xs={0} sm={1} className="d-none d-sm-block text-center">
                =>
            </Col>
            <Col xs={12} sm={5} md={5} lg={3} className="mb-1">
                <MoneyInput
                    currencies={currencies}
                    selectedCurrency={exchangeStatus.receiving.currency}
                    amount={exchangeStatus.receiving.amount}
                    onChange={data => setExchangeStatus({ ...data, type: RECEIVING })}
                    clearOnFocus={exchangeStatus.calculated === RECEIVING}
                />
            </Col>
            <Col md={6} lg={3} className="mb-1">
                <Button onClick={handleSubmit}>
                    Exchange 
                    {isLoading && <Spinner animation="border" size="sm" className="ml-1" />}
                </Button>
            </Col>
        </Row>
    );
};

CurrencyExchange.propTypes = {
    rates: PropTypes.object.isRequired,
    currencies: PropTypes.array.isRequired,
    defaultCurrency: PropTypes.string.isRequired,
    validateExchange: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    rates: selectExchangeRates(state),
    currencies: selectCurrencies(state),
    defaultCurrency: selectDefaultCurrency(state),
});

export default connect(
    mapStateToProps,
    { validateExchange }
)(CurrencyExchange)
